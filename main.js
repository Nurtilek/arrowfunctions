const fruits = ['apple', 'strawberry', 'banana', 'melon', 'orange']

const isHasA = fruits.some((fruit)=> fruit.includes('a') && fruit.length < 6)

if(isHasA){
    console.log('Вон, же apple написано, че сам посмотреть не можешь, кожанный!')
}

const isBigger = fruits.every((fruit)=> fruit.length > 3)

if(isBigger){
    console.log('Считать научись, видно же, что там больше 3х')
}

const isThereA = fruits.find((fruit)=>fruit.includes('g'))

if(isThereA){
    console.log('Ты читать разучился? Вон же в слове "orange" есть "g"')
}